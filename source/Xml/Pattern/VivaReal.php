<?php


namespace Parser\Xml\Pattern;


use Parser\Xml\Xml;

class VivaReal extends Xml
{

    protected function getXml()
    {
        if ($this->fileUrl) {
            $this->curl->get($this->fileUrl);

            if ($this->curl->error) {
                $this->resetResult();
//                $this->errorCode = 1;
//                $this->errorMessage = 'Oppsss, tivemos problemas ao acessar o link enviado!';
                return false;
            } else {
                if (!empty($this->curl->response->Listings->Listing)) {
                    $this->rawResult = $this->curl->response->Listings->Listing;
                    return true;
                } else {
                    $this->resetResult();
//                    $this->errorCode = 2;
//                    $this->errorMessage = 'Oppsss, o link enviado não parece ser de um xml no padrão VivaReal!';
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    protected function parserContent()
    {
        foreach ($this->rawResult as $item) {
            $arr = [];
            $arr['realty_register'] = preg_replace('/[^a-zA-Z0-9]+/', "", $this->getContent($item->Details->insc_imobil));
            $arr['realty_created'] = date_parser($this->getContent($item->Details->dt_cadastro));

            if (empty($arr['realty_register']) || empty($arr['realty_created'])) {
                unset($arr);
                continue;
            }

            $arr['realty_ref'] = $this->getContent($item->ListingID);
            $arr['realty_transaction'] = $this->getContent($item->TransactionType);
            $arr['realty_type'] = parserRealtyType($this->getContent($item->Details->PropertyType));
            $arr['realty_type_name'] = str_slug(getRealtyType($arr['realty_type']));
            $arr['realty_desc'] = str_replace(array('&lt;', '&gt;'), array(' <', '> '), $this->getContent($item->Details->Description));
            $arr['realty_listprice'] = $this->getContent($item->Details->ListPrice);
            $arr['realty_rentalprice'] = $this->getContent($item->Details->RentalPrice);
            $arr['realty_administration_fee'] = $this->getContent($item->Details->PropertyAdministrationFee);
            $arr['realty_yearly_tax'] = $this->getContent($item->Details->YearlyTax);
            $arr['realty_bedrooms'] = $this->getContent($item->Details->Bedrooms);
            $arr['realty_bathrooms'] = $this->getContent($item->Details->Bathrooms);
            $arr['realty_apartments'] = $this->getContent($item->Details->Suites);
            $arr['realty_parkings'] = $this->getContent($item->Details->Garage);
            $arr['realty_builtarea'] = $this->getContent($item->Details->ConstructedArea);
            $arr['realty_livingarea'] = $this->getContent($item->Details->LivingArea);
            $arr['realty_totalarea'] = $this->getContent($item->Details->LotArea);

            //ADDR
            $arr['realty_addr_state'] = $this->getContent($item->Location->State);
            $arr['realty_addr_state_uf'] = $this->getContent($item->Location->State);; //ATRIBUTO = abbreviation
            $arr['realty_addr_city'] = $this->getContent($item->Location->City);
            $arr['realty_addr_city_name'] = str_slug($arr['realty_addr_city']);
            $arr['realty_addr_zone'] = $this->getContent($item->Location->Zone);
            $arr['realty_addr_district'] = $this->getContent($item->Location->Neighborhood);
            $arr['realty_addr_district_name'] = str_slug($arr['realty_addr_district']);
            $arr['realty_addr'] = $this->getContent($item->Location->Address);
            $arr['realty_addr_district'] = $this->getContent($item->Location->Neighborhood);
            $arr['realty_addr_number'] = $this->getContent($item->Location->StreetNumber);
            $arr['realty_addr_postalcode'] = $this->getContent($item->Location->PostalCode);
            $arr['realty_addr_latitude'] = $this->getContent($item->Location->Latitude);
            $arr['realty_addr_longitude'] = $this->getContent($item->Location->Longitude);

            //TITLE
            $arr['realty_title'] = getRealtyType($arr['realty_type']);
            $arr['realty_title'] .= (!empty($arr['realty_bedrooms']) ? (" com {$arr['realty_bedrooms']} quarto" . ($arr['realty_bedrooms'] > 1 ? 's' : '')) : '');
            $arr['realty_title'] .= (($arr['realty_transaction'] == 'For Sale' || $arr['realty_transaction'] == 'Sale/Rent') ? ' à venda' : ($arr['realty_transaction'] == 'For Rent' ? ' para locação' : ''));
            $arr['realty_title'] .= ' no ' . ucwords(mb_strtolower($arr['realty_addr_district'], 'UTF-8')) . ', ' . ucwords(mb_strtolower($arr['realty_addr_city'], 'UTF-8')) . " ({$arr['realty_ref']})";

            $arr['realty_name'] = str_slug($arr['realty_title']);


            //FEATURES
            if (!empty($item->Details->Features->Feature && is_object($item->Details->Features->Feature))) {
                foreach ($item->Details->Features->Feature as $feature) {
                    $arr['realty_features'][] = $this->getContent($feature);
                }
            } else {
                $arr['realty_features'] = null;
            }

            //MEDIA
            if (!empty($item->Media->Item)) {
                foreach ($item->Media->Item as $media) {
                    if ($this->getContent($media) && !empty($media->attributes()['video'])) {
                        $arr['realty_video'] = $this->getContent($media);
                    } elseif ($this->getContent($media) && !empty($media->attributes()['primary']) && !empty($media->attributes()['medium']) && $media->attributes()['medium'] == 'image') {
                        $arr['realty_cover'] = str_replace(' ', '%20', $this->getContent($media));
                        $arr['realty_images'][] = str_replace(' ', '%20', $this->getContent($media));
                    } elseif (!empty($media->attributes()['medium']) && $media->attributes()['medium'] == 'image') {
                        $arr['realty_images'][] = str_replace(' ', '%20', $this->getContent($media));
                    }
                }

                if (empty($arr['realty_images'])) {
                    $arr['realty_images'] = null;
                    $arr['realty_cover'] = null;
                } elseif (empty($arr['realty_cover']) && !empty($arr['realty_images'][0])) {
                    $arr['realty_cover'] = $arr['realty_images'][0];
                }

            } else {
                $arr['realty_images'] = null;
                $arr['realty_cover'] = null;
                $arr['realty_video'] = null;
            }

            //STATUS
            $arr['realty_status'] = (!empty($arr['realty_cover']) ? 1 : 0);
//            $arr['realty_realstate_id'] = $this->imobi;
            $this->result[] = $arr;

            $this->total++;
        }
    }
}