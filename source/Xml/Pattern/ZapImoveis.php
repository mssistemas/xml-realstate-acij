<?php


namespace Parser\Xml\Pattern;


use Parser\Xml\Xml;

class ZapImoveis extends Xml
{

    protected function getXml()
    {
        if ($this->fileUrl) {
            $this->curl->get($this->fileUrl);

            if ($this->curl->error) {
                $this->resetResult();
//                $this->errorCode = 1;
//                $this->errorMessage = 'Oppsss, tivemos problemas ao acessar o link enviado!';
                return false;
            } else {
                if ($this->curl->response->tipo && $this->curl->response->tipo == 'Aviso') {
                    exit;
                } elseif (!empty($this->curl->response->Imovel)) {
                    $this->rawResult = $this->curl->response->Imovel;
                    return true;
                } else {
                    $this->resetResult();
//                    $this->errorCode = 2;
//                    $this->errorMessage = 'Oppsss, o link enviado não parece ser de um xml no padrão VivaReal!';
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    protected function parserContent()
    {
        foreach ($this->rawResult as $item) {
            $arr = [];
            $arr['realty_register'] = preg_replace('/[^a-zA-Z0-9]+/', "", $this->getContent($item->InscricaoMunicipal));
            $arr['realty_created'] = date_parser($this->getContent($item->DataCadastro));

            if (empty($arr['realty_register']) || empty($arr['realty_created'])) {
                unset($arr);
                continue;
            }

            $arr['realty_ref'] = $this->getContent($item->CodigoImovel);

            //VENDA + LOCAÇÃO
            if ($this->getContent($item->Venda) == 'Sim' && $this->getContent($item->Locacao) == 'Sim') {
                $arr['realty_transaction'] = 'Sale/Rent';

                //VENDA + LOCAÇÃO
            } elseif ($this->getContent($item->Venda) == 'Sim' && $this->getContent($item->LocacaoTemporada) == 'Sim') {
                $arr['realty_transaction'] = 'Sale/Rent';

                //LOCAÇÃO
            } elseif ($this->getContent($item->Locacao) == 'Sim' || $this->getContent($item->LocacaoTemporada) == 'Sim') {
                $arr['realty_transaction'] = 'For Rent';

                //VENDA
            } elseif ($this->getContent($item->Venda) == 'Sim') {
                $arr['realty_transaction'] = 'For Sale';

                //VENDA + LOCAÇÃO
            } else {
                $arr['realty_transaction'] = 'Sale/Rent';
            };


            $arr['realty_type'] = parserRealtyType($this->getContent($item->TipoImovel));
            $arr['realty_type_name'] = str_slug(getRealtyType($arr['realty_type']));
            $arr['realty_desc'] = str_replace(array('&lt;', '&gt;'), array(' <', '> '), $this->getContent($item->Descricao));
            $arr['realty_listprice'] = $this->getContent($item->PrecoVenda);
            $arr['realty_rentalprice'] = $this->getContent($item->PrecoLocacao);
            $arr['realty_rentalprice'] = $this->getContent($item->PrecoLocacao);
            $arr['realty_administration_fee'] = $this->getContent($item->PrecoCondominio);
            $arr['realty_yearly_tax'] = $this->getContent($item->ValorIptu);
            $arr['realty_bedrooms'] = $this->getContent($item->QtdDormitorios);
            $arr['realty_bathrooms'] = $this->getContent($item->QtdBanheiros);
            $arr['realty_apartments'] = $this->getContent($item->QtdSuites);
            $arr['realty_parkings'] = $this->getContent($item->QtdVagas);
            $arr['realty_builtarea'] = $this->getContent($item->AreaTotal);
            $arr['realty_livingarea'] = $this->getContent($item->AreaUtil);
            $arr['realty_totalarea'] = $this->getContent($item->AreaTotal);

            //ADDR
            $arr['realty_addr_state'] = $this->getContent($item->UF);
            $arr['realty_addr_state_uf'] = $this->getContent($item->UF);
            $arr['realty_addr_city'] = $this->getContent($item->Cidade);
            $arr['realty_addr_city_name'] = str_slug($arr['realty_addr_city']);
//            $arr['realty_addr_zone'] = $this->getContent($item->Location->Zone);
            $arr['realty_addr_district'] = $this->getContent($item->Bairro);
            $arr['realty_addr_district_name'] = str_slug($arr['realty_addr_district']);
            $arr['realty_addr'] = $this->getContent($item->Location->Endereco);
            $arr['realty_addr_number'] = $this->getContent($item->Numero);
            $arr['realty_addr_postalcode'] = $this->getContent($item->CEP);
            $arr['realty_addr_latitude'] = $this->getContent($item->GMapsLatitude);
            $arr['realty_addr_longitude'] = $this->getContent($item->GMapsLongitude);

            //TITLE
//            $arr['realty_title'] = getWcRealtyType($arr['realty_type']);
            $arr['realty_title'] = $arr['realty_type'];
            $arr['realty_title'] .= (!empty($arr['realty_bedrooms']) ? (" com {$arr['realty_bedrooms']} quarto" . ($arr['realty_bedrooms'] > 1 ? 's' : '')) : '');
            $arr['realty_title'] .= (($arr['realty_transaction'] == 'For Sale' || $arr['realty_transaction'] == 'Sale/Rent') ? ' à venda' : ($arr['realty_transaction'] == 'For Rent' ? ' para locação' : ''));
            $arr['realty_title'] .= ' no ' . ucwords(mb_strtolower($arr['realty_addr_district'], 'UTF-8')) . ', ' . ucwords(mb_strtolower($arr['realty_addr_city'], 'UTF-8')) . " ({$arr['realty_ref']})";

            $arr['realty_name'] = str_slug($arr['realty_title']);

            //FEATURES
            $arr['features'] = null;
            foreach ($item as $key => $value) {
                if (getFeature($key) && $this->getContent($value) == 'Sim') {
                    $arr['features'][] = $key;
                }
            }

            //MEDIA
            if (!empty($item->Fotos->Foto) || !empty($item->Videos->Video)) {
                foreach ($item->Fotos->Foto as $media) {
                    $arr['realty_images'][] = str_replace(' ', '%20', $this->getContent($media->URL));
                }

                foreach ($item->Videos->Video as $media) {
                    $arr['realty_video'] = $this->getContent($media->URL);
                }

                if (empty($arr['realty_images'])) {
                    $arr['realty_images'] = null;
                    $arr['realty_cover'] = null;
                } elseif (empty($arr['realty_cover']) && !empty($arr['realty_images'][0])) {
                    $arr['realty_cover'] = $arr['realty_images'][0];
                }

            } else {
                $arr['realty_images'] = null;
                $arr['realty_cover'] = null;
                $arr['realty_video'] = null;
            }

            //STATUS
            $arr['realty_status'] = (!empty($arr['realty_cover']) ? 1 : 0);

            $this->result[] = $arr;

            $this->total++;
        }
    }
}