<?php


namespace Parser\Xml;

use Curl\Curl;

require_once __DIR__ . "/../Core/Helpers.php";
require_once __DIR__ . "/../Core/Parser.php";

abstract class Xml
{
    /**
     * URL do xml
     * @var string
     */
    protected $fileUrl;

    /**
     * Nome da empresa que o xml será integrado
     * @var string
     */
    protected $pattern;

    /**
     * Número total de imóveis no xml
     * @var int
     */
    protected $total;

    /**
     * Número total de imóveis que possuem requisitos para serem cadastrados no banco de dados
     * @var int
     */
    protected $totalApproved;

    /**
     * Resultado já montado os imóveis
     * @var array
     */
    protected $result;

    /**
     * Resultado "cru" da forma que é recebido pelo CURL
     * @var mixed
     */
    protected $rawResult;

    /**
     * Error
     * $error->status = true/false;
     * $error->code = 1,2,3,4,5....
     * $error->message = 'lorem lorem lorem'
     * @var object
     */
    protected $error;

    /**
     * Atributo usando para o controle das chamadas CURL
     * @var Curl
     */
    protected $curl;

    public function __construct()
    {
        $this->resetError();
        $this->curl = new Curl();
    }

    /**
     * Método responsável por fazer uma chamada CURL e validar se o xml recebido é valido
     * @return boolean
     */
    protected abstract function getXml();

    /**
     * Método responsável por alterar os nomes dos índices dos imóveis para o padrão utilizado pelo Portal de Imóveis ACIJ
     * @return mixed
     */
    protected abstract function parserContent();

    /**
     * Método responsável por receber a URL do xml e retorna-lo formatado
     * @param string $fileUrl = URL do xml
     * @return boolean
     */
    public function exeRead($fileUrl)
    {
        $this->resetError();
        $this->resetResult();

        if (!empty($fileUrl)) {
            $this->fileUrl = $fileUrl;
            if ($this->getXml()) {
                $this->parserContent();
                return true;
            } else {
                return false;
            }
        } else {
            $this->fileUrl = null;

//            $this->error->message = 'Oppsss, o link enviado não parece ser de um xml padrão VivaReal!';
            return false;
        }
    }

    /**
     * Método responsável por filtrar e retornar o conteudo de um parâmetro passado ou mesmo de um atributo do parâmetro
     * @param $content = Objeto xml ao qual o valor deve se filtrado e retornado
     * @param null $attribute = Nome do attributo ao qual se quer obter o valor
     * @return mixed
     */
    protected function getContent($content, $attribute = null)
    {
        if (!empty($content) && $attribute) {

        } elseif (!empty($content)) {
            if (is_string($content) || is_float($content) || is_int($content)) {
                return filter_var($content, FILTER_SANITIZE_STRIPPED);
            } elseif (is_object($content) && !empty($content->__tostring())) {
                return filter_var($content->__tostring(), FILTER_SANITIZE_STRIPPED);
            } elseif (is_array($content)) {
                return filter_var_array($content, FILTER_SANITIZE_STRIPPED);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Método responsável por setar valores padrões aos erros
     */
    protected function resetError()
    {
        $this->error = new \StdClass();
        $this->error->status = false;
        $this->error->code = null;
        $this->error->menssage = null;
    }

    protected function resetResult()
    {
        $this->result = null;
        $this->rawResult = null;
        $this->total = 0;
        $this->totalApproved = 0;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @return int
     */
    public function getTotalApproved()
    {
        return $this->totalApproved;
    }

    /**
     * @return array
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return mixed
     */
    public function getRawResult()
    {
        return $this->rawResult;
    }

    /**
     * @return object
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Método clone do tipo privado previne a clonagem dessa instância
     * da classe
     *
     * @return void
     */
    private function __clone()
    {

    }

    /**
     * Método unserialize do tipo privado para previnir que desserialização
     * da instância dessa classe.
     *
     * @return void
     */
    private function __wakeup()
    {

    }
}