<?php

/**
 * ##################
 * ###   STRING   ###
 * ##################
 */

/**
 * Função responsável por transformar uma string em uma slug para ser
 * usada principalmente como url
 * @param $string
 * @return string
 */
function str_slug($string)
{
    $string = filter_var(mb_strtolower($string), FILTER_SANITIZE_STRIPPED);
    $formats = '¹²³ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()_-+={[}]/?;:.,\\\'<>°ºª';
    $replace = '123aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                                 ';

    $slug = str_replace(["-----", "----", "---", "--"], "-",
        str_replace(" ", "-",
            trim(strtr(utf8_decode($string), utf8_decode($formats), $replace))
        )
    );

    return $slug;
}

/**
 * ################
 * ###   DATE   ###
 * ################
 */

/**
 * Transforma uma data no formato DD/MM/YY em uma data no formato TIMESTAMP!
 * @param $date
 * @return DateTime
 */
function date_parser($date)
{
    $timestamp = null;
    if (strstr($date, '-')) {
        $timestamp = \DateTime::createFromFormat('d-m-Y', $date);
    } elseif (strstr($date, '/')) {
        $timestamp = \DateTime::createFromFormat('d/m/Y', $date);
    } else {
        return null;
    }
    return $timestamp->format('Y-m-d H:i:s');
}