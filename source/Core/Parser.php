<?php

/**
 * Função responsável por normalizar todos os tipos
 * de imóveis passando para o padrão da VivaReal
 * @param string $type
 * @return null|string
 */
function parserRealtyType($type)
{
    if (!empty($type) && is_string($type)) {
        switch ($type) {
            case 'Residential / Apartment':
            case 'Residential/Apartment':
            case 'Apartamentos':
                return 'Residential / Apartment';
                break;

            case 'Residential / Home':
            case 'Residential/Home':
            case 'Casa':
                return 'Residential / Home';
                break;

            case 'Residential / Farm Ranch':
            case 'Residential / Farm/Ranch':
            case 'Chácara':
            case 'Sitio':
            case 'Pousada/Hotel':
                return 'Residential / Farm Ranch';
                break;

            case 'Residential / Condo':
            case 'Residential/Condo':
            case 'Casa em Condominio':
                return 'Residential / Condo';
                break;

            case 'Residential / Flat':
            case 'Residential/Flat':
            case 'Flat':
                return 'Residential / Flat';
                break;

            case 'Residential / Land Lot':
            case 'Residential / Land/Lot':
            case 'Residential/Land Lot':
            case 'Residential/Land/Lot':
            case 'Terreno':
                return 'Residential / Land Lot';
                break;

            case 'Residential / Sobrado':
            case 'Residential/Sobrado':
            case 'Sobrado':
                return 'Residential / Sobrado';
                break;

            case 'Residential / Penthouse':
            case 'Residential/Penthouse':
            case 'Coberturas':
                return 'Residential / Penthouse';
                break;

            case 'Residential / Kitnet':
            case 'Residential/Kitnet':
            case 'Salas/Conjuntos':
                return 'Residential / Kitnet';
                break;

            case 'Commercial / Consultorio':
                return 'Commercial / Consultorio';
                break;

            case 'Commercial / Office':
            case 'Commercial/Office':
                return 'Commercial / Office';
                break;

            case 'Commercial / Agricultural':
            case 'Commercial/Agricultural':
                return 'Commercial / Agricultural';
                break;

            case 'Comercial / Industrial':
            case 'Commercial / Industrial':
            case 'Commercial/Industrial':
            case 'Deposito':
            case 'Deposito':
            case 'Pavilhão':
            case 'Galpão':
            case 'Galpão para condomínio':
            case 'Galpão Industrial':
                return 'Comercial / Industrial';
                break;

            case 'Comercial / Building':
            case 'Commercial / Building':
            case 'Commercial/Building':
            case 'Casa comercial':
                return 'Comercial / Building';
                break;

            case 'Commercial / Loja':
            case 'Comercial / Loja':
            case 'Commercial/Loja':
            case 'Comercial/Loja':
            case 'Loja':
                return 'Commercial / Loja';
                break;

            case 'Commercial / Land Lot':
            case 'Comercial / Land Lot':
            case 'Commercial/Land Lot':
            case 'Commercial / Land/Lot':
            case 'Commercial/Land/Lot':
            case 'Loteamento Industrial':
            case 'Terreno Industrial':
            case 'Terreno Comercial':
                return 'Commercial / Land Lot';
                break;

            case 'Commercial / Business':
            case 'Commercial/Business':
            case 'Ponto Comercial':
                return 'Commercial / Business';
                break;

            case 'Commercial / Residential Income':
            case 'Commercial/Residential Income':
            case 'Prédio':
            case 'Predio Comercial':
            case 'Prédio Comercial':
            case 'Andar Inteiro':
            case 'Meio Andar':
                return 'Commercial / Residential Income';
                break;

            case 'Coworking':
                return 'Coworking';
                break;

            case 'Box':
            case 'Garagem':
                return 'Box / Parking';
                break;

            case 'Loft':
                return 'Loft';
                break;

            default:
                return 'Indefinido';
                break;
        }
    } else {
        return null;
    }
}

function getRealtyType($type)
{
    if (!empty($type) && is_string($type)) {
        switch ($type) {
            case 'Residential / Apartment':
                return 'Apartamento';
                break;

            case 'Residential / Home':
                return 'Casa';
                break;

            case 'Residential / Farm Ranch':
                return 'Chácara';
                break;

            case 'Residential / Condo':
                return 'Casa de Condomínio';
                break;

            case 'Residential / Flat':
                return 'Flat';
                break;

            case 'Residential / Land Lot':
                return 'Lote/Terreno';
                break;

            case 'Residential / Sobrado':
                return 'Sobrado';
                break;

            case 'Residential / Penthouse':
                return 'Cobertra';
                break;

            case 'Residential / Kitnet':
                return 'Kitnet';
                break;

            case 'Commercial / Consultorio':
                return 'Consultório';
                break;

            case 'Commercial / Office':
                return 'Sala Comercial';
                break;

            case 'Commercial / Agricultural':
                return 'Fazenda/Sítio';
                break;

            case 'Comercial / Industrial':
                return 'Glapão/Depósito/Armazém';
                break;

            case 'Comercial / Building':
                return 'Imóvel Comercial';
                break;

            case 'Commercial / Loja':
                return 'Commercial / Loja';
                break;

            case 'Commercial / Land Lot':
                return 'Commercial / Land Lot';
                break;

            case 'Commercial / Business':
                return 'Commercial / Business';
                break;

            case 'Commercial / Residential Income':
                return 'Commercial / Residential Income';
                break;

            case 'Coworking':
                return 'Coworking';
                break;

            case 'Box / Parking':
                return 'Box/Garagem';
                break;

            case 'Loft':
                return 'Loft';
                break;

            default:
                return 'Indefinido';
                break;
        }
    }
}

/**
 * Função responsável por receber uma string e verificar se ela é
 * uma caracteristica do imóvel (composição/infraestrutura)
 * @param string $feature
 * @return bool
 */
function getFeature(string $feature)
{
    $features = [
        'Acesso24Horas',
        'Agua',
        'ArCondicionado',
        'ArmarioCozinha',
        'ArmarioEmbutido',
        'BusinessCenter',
        'Cerca',
        'Churrasqueira',
        'CoffeeShop',
        'Convencoes',
        'Copa',
        'EntradaCaminhoes',
        'Endereco',
        'Escritorio',
        'EscritorioVirtual',
        'Esgoto',
        'Esquina',
        'EstacionamentoRotativo',
        'EstacionamentoVisitantes',
        'EstradaAsfaltada',
        'GaragemBarcos',
        'Guarita',
        'Heliponto',
        'InfraInternet',
        'Jardim',
        'Lago',
        'Lavoura',
        'Luz',
        'Marina',
        'Mezanino',
        'Pasto',
        'PatioEstacionamento',
        'Pier',
        'Piscina',
        'PisoElevado',
        'PistaPouso',
        'Playground',
        'PonteRolante',
        'QuadraSquash',
        'QuadraTenis',
        'QuadraPoliEsportiva',
        'Quintal',
        'TVCabo',
        'Varanda',
        'Vestiario',
        'VidrosReflexivos',
        'WCEmpregada',
        'QuartoWCEmpregada',
        'Lavabo',
        'EstudaPermuta',
        'DepositoSubsolo',
        'Closet',
        'Hidromassagem',
        'Lareira',
        'FrenteMar',
        'AndarInteiro',
        'AreaServico',
        'Bosque',
        'CampoFutebol',
        'CasaCaseiro',
        'CasaFundo',
        'CasaPrincipal',
        'Caseiro',
        'ComServico',
        'CozinhaAzulejada',
        'Despensa',
        'EnergiaEletrica',
        'EntradaServicoIndependente',
        'EntradaFacilitada',
        'EntradaLateral',
        'Fogao',
        'Freezer',
        'Geminada',
        'HomeTheater',
        'ImovelExposicao',
        'Interfone',
        'LavaRoupas',
        'LavanderiaColetiva',
        'Isolada',
        'LivingTabuasLargas',
        'MeioAndar',
        'Microondas',
        'Oferta',
        'Paiol',
        'ParaIncorporacao',
        'PistaCooper',
        'PistaSkate',
        'Poco',
        'PocoArtesiano',
        'ProntoMorar',
        'Refeitorio',
        'RoupaBanho',
        'RoupaCama',
        'RoupaMesa',
        'SalaAlmoco',
        'SalaJantar',
        'SalaIntima',
        'SalaVideo',
        'SalaoFestas',
        'SegurancaInterna',
        'Semigeminada',
        'SpaHidromassagem',
        'StandVendasLocal',
        'TV',
        'UtensiliosCozinha',
        'UtilizeFGTS',
        'VentiladoresTeto',
        'VisiteImovelDecorado',
        'Clube',
        'ChildrenCare',
        'Curral',
        'Mobiliado',
        'Recuo',
        'Solarium',
        'CasaMista',
        'CasaAlvenaria',
        'CasaMadeira',
        'Terraco',
        'TipoOferta',
        'ValorEntrada',
        'ValorMensal',
        'DescricaoLocalizacao',
        'EspacoGourmet',
        'Laje',
        'VistaPanoramica',
        'AreaLazer',
        'Academia',
        'VentilacaoNatural',
        'Banheira',
        'Platibanda',
        'JanelasGrandes',
        'JanelaAluminio',
        'MuroVidro',
        'Balaustre',
        'Escada',
        'Drywall',
        'GasEncanado',
        'Rampas',
        'Arandelas',
        'CimentoQueimado',
        'GessoSanca',
        'Acessibilidade',
        'Aquario',
        'IsolamentoAcustico',
        'IsolamentoTermico',
        'Porcelanato',
        'PisoLaminado',
        'PisoMadeira',
        'PisoVinilico',
        'PapelParede',
        'MoveisPlanejados',
        'Ofuro',
        'MurosGrades',
        'Blindex',
        'CozinhaAmericana',
        'AmbientesIntegrados',
        'SalaPequena',
        'SalaGrande',
        'Edicula',
        'HallEntrada',
        'CozinhaGrande',
        'Biblioteca',
        'Horta',
        'Orquidario',
        'VarandaGourmet',
        'ChurrasqueiraVaranda',
        'CozinhaPequena',
        'RedeTelefone',
        'ReservatorioAgua',
        'Restaurante',
        'Rio',
        'RuaAsfaltada',
        'SalaGinastica',
        'SalaoJogos',
        'Sauna',
        'Sede',
        'SegurancaRua',
        'SegurancaPatrimonial',
        'Silos',
        'SistemaIncendio',
        'Telefone',
        'PorteiraFechada'
    ];

    if (in_array($feature, $features)) {
        return true;
    } else {
        return false;
    }
}